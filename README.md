# VerilogDC

This project is still in very early WIP stage!

VerilogDC is an open-source Dreamcast compatible implementation in portable Verilog RTL.

## CPU

This project implements a modified SH4 CPU core with the following specs:

- 4-stage dual-issue pipeline
- I/D cache WIP
- No MMU
- 16-bit fixed-length instruction set
- FPU WIP
- Support all SH7750 instructions except MMU related ones

### Frontend

The frontend is really simple, it fetches 32-bit per cycle, which contains 2 instructions. There is no instruction buffer, no prefetch queue, no branch predictor, nothing. When a branch is taken, it's simply redirected and generally incur a one cycle penalty. When dual issue is not possible (only 16-bit is consumed in the 32-bit fetch packet), it's simply re-fetched without introducing any additional bubbles.

### Function Units and Issue Ports

There are 7 function units:
- MT0 and MT1 unit, for comparison instructions
- EX unit, for integer arithmetic instructions
- BR unit, for branching instructions
- LS unit, for memory load/ store instructions
- FE unit, for floating point instructions
- CSR unit, for special register handling

There are 2 issue ports, one for the lower 16bit and one for the upper 16bit of a 32bit fetch packet.

MT0 is statically assigned to issue port 0, and MT1 is statically assigned to issue port 1, while all other units have a mux to select between two issue ports. This is necessary as SH4 doesn't have a strict pipeline pairing rule: while issue count is limited to 1 per cycle, there is generally no restrictions of which two types of instructions could be co-issued.

### Memory Subsystem

Both I-cache and D-cache has 1 cycle latency. For load-use latency, d-cache address is sent out in E1, result comes back in E2, the E2 enters forwarding network in ID and has to be registered to be used in EX. This is generally reasonable on ASIC, but maybe not necessary on FPGA.

### Modifications

As a recap, the SH4 CPU used on dreamcast has the following features:

- 5-stage dual-issue pipeline
- 8KB direct-mapped I-cache
- 16KB direct-mapped D-cache (called operand cache)
- 4-entry fully-associative ITLB
- 64-entry fully-associative UTLB
- 16-bit fixed-length instruction
- IEEE 754 single precision FPU with hardware DP emulation

The following are the differences between implementation in this project and the original SH4:

1. Pipeline has been shorten to 4 stages instead of 5-stage: Shortening the pipeline is possible due to moving the forwarding network from EX to ID, so memory result doesn't need to be registered to another pipeline stage for forwarding. This allows the forwarding network to be simpler, however the exact cycle time impact is unknown for now.
2. MMU is not supported. MMU is only used in Windows CE games, while it's good to have I decided to leave it out for now. The SH4 MMU doesn't have hardware PTW, but only software refilled TLBs, which should be rather easy to do. Supporting MMU likely requires a VIPT cache as well. The TLB is rather large at 64-entry fully associative would be too slow to look up if it's stuffed into combinational path of a PIPT cache of only 1-cycle latency.
3. Memory load with index post incrementing is not allowed to co-issue with other instructions. This particular instruction writes 2 register results: one load result and one incremented index. In the SH7750 manual, this instruction belongs to LS group, means it can be co-issued with other instructions from other groups other than CO group. This would means the register file needs to have another write port just to support the co-issue of this instruction. I am disallowing this to save 1 write port on RF.
4. Memory store using register base + register indexed addressing mode is not allowed to co-issue with other instructions. These instructions read 3 register values, supporting them would require additional RF read port. While the manual indicates that these could be co-issued with other instructions, I am disallowing that.

## Building

Currently the only supported building target is the verilator-based cosimulator, which runs the RTL model and the washDC dreamcast emulator in lock-step for error checking, as well as emulating things not (yet) implemented in VerilogDC.

### Cosimulator

The only supported building environment for the co-simulator is Linux.

Make sure Verilator 5.0+ and [Verible](https://github.com/chipsalliance/verible/releases) are installed and added to PATH (among other normal dependencies like gcc, cmake, etc.).

```
git clone --recursive https://gitlab.com/zephray/VerilogDC
cd VerilogDC/cosim/washdc-cosim/
mkdir build
cmake ..
make
cd ../../
make
```

Then you could try run things (and see it breaks):

```
./simulator --trace --game /opt/toolchains/dc/kos/examples/dreamcast/hello/hello.elf
```

## Acknowledgements

During the design of this processor, I have used the following projects as reference:

- [lowRISC's muntjac](https://github.com/lowRISC/muntjac), Apache 2.0 license
- [Taner Öksüz's fpu-sp](https://github.com/taneroksuz/fpu-sp), Apache 2.0 license

The [WashDC](https://gitlab.com/washemu/washdc), GNU GPL3 license is used in the cosimulator.

## License

Unless otherwise specified, the source code is licensed under MIT. Noteably, all RTL code are licensed under MIT.

The co-simulator links against the washDC dreamcast emulator, which is licensed under GPL. The cosimulator is also licensed under GPL.
