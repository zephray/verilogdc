#
# Manjuu
# Copyright 2023 Wenting Zhang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

from manjuu_base import define, reverse

romem_if_t = [
    ["o", "req_addr", 32],
    ["o", "req_valid"],
    ["i", "resp_rdata", 32],
    ["i", "resp_valid"]
]

rwmem_if_t = [
    ["o", "req_addr", 32],
    ["o", "req_wdata", 64],
    ["o", "req_wmask", 8],
    ["o", "req_wen"],
    ["o", "req_valid"],
    ["i", "resp_rdata", 64],
    ["i", "resp_valid"]
]

rf_rd_t = [
    ["i", "rsrc", 4],
    ["o", "rdata", 32],
    ["i", "rbank"]
]

rf_wr_t = [
    ["i", "wen"],
    ["i", "wdst", 4],
    ["i", "wdata", 32]
]

fprf_rd_t = [
    ["i", "rsrc", 4],
    ["i", "rbank"],
    ["o", "rdata", 32]
]

fprf_wr_t = [
    ["i", "wen"],
    ["i", "wdst", 4],
    ["i", "wbank"],
    ["i", "wdata", 32]
]

dec_in_t = [
    ["i", "valid"],
    ["i", "npc", 32],
    ["i", "raw", 16],
    ["i", "rl", 32],
    ["i", "rh", 32],
    ["i", "r0", 32]
]

dec_out_t = [
    ["o", "opl", 32],
    ["o", "oph", 32],
    ["o", "use_r0"],
    ["o", "use_rl"],
    ["o", "use_rh"],
    ["o", "write_rn"],
    ["o", "write_r0"],
    ["o", "fp_use_rl"],
    ["o", "fp_use_rh"],
    ["o", "fp_use_r0"],
    ["o", "fp_ls_use_freg"],
    ["o", "fp_ls_use_altbank"],
    ["o", "fp_ls_use_rh"],
    ["o", "fp_write_rn"],
    ["o", "fp_write_altbank"],
    ["o", "fp_op", 4],
    ["o", "use_fpul"],
    ["o", "write_fpul"],
    ["o", "use_csr"],
    ["o", "write_csr"], 
    ["o", "is_mt"],
    ["o", "is_ex"],
    ["o", "is_br"],
    ["o", "is_ls"],
    ["o", "is_fp"],
    ["o", "csr_id", 4],
    ["o", "raltbank"],
    ["o", "complex"],
    ["o", "use_t"],
    ["o", "write_t"],
    ["o", "legal"]
]

common_fu_in_t = [
    ["i", "raw", 16],
    ["i", "opl", 32],
    ["i", "oph", 32]
]

exu_in_t = [
    ["i", "valid"],
    ["i", "flags", 4]
] + common_fu_in_t

exu_out_t = [
    ["o", "flags", 4]
] + reverse(rf_wr_t)

bru_in_t = [
    ["i", "valid"],
    ["i", "pr", 32],
    ["i", "t"]
] + common_fu_in_t

bru_out_t = [
    ["o", "taken"],
    ["o", "target", 32],
    ["o", "delayslot"],
    ["o", "write_pr"]
]

mtu_in_t = [
    ["i", "valid"],
] + common_fu_in_t

mtu_out_t = [
    ["o", "t"]
] + reverse(rf_wr_t)

lsu_in_t = [
    ["i", "valid"],
    ["i", "csr_rdata", 32],
    ["i", "r0", 32],
    ["i", "fpr", 32]
] + common_fu_in_t

# FPUL in lsu output is for hazard checking purposes only,
# actual WB in E2 stage csr_wdata
lsu_e1_out_t = [
    ["o", "alt_wen"],
    ["o", "alt_wdst", 4],
    ["o", "alt_wdata", 32],
    ["o", "fpul_wen"],
    ["o", "wpending"],
    ["o", "wfp"]
] + reverse(rf_wr_t)

lsu_e2_out_t = [
    ["o", "csr_wen"],
    ["o", "csr_wdata", 32],
    ["o", "csr_tonly"],
    ["o", "nack"],
    ["o", "fpul_wen"],
    ["o", "waltbank"],
    ["o", "wfp"]
] + reverse(rf_wr_t)

fpu_in_t = [
    ["i", "valid"],
    ["i", "raw", 16],
    ["i", "fop", 4],
    ["i", "frl", 32],
    ["i", "frh", 32],
    ["i", "fr0", 32]
]

fpu_out_t = [
    ["o", "valid"],
    ["o", "t"],
    ["o", "t_wen"],
    ["o", "fpul", 32],
    ["o", "fpul_wen"]
] + reverse(fprf_wr_t)

retire_t = [
    ["o", "valid"],
    ["o", "pc", 32],
    ["o", "raw", 16],
    ["o", "flags", 4]
]

completion_t = [
    ["o", "wpending"]
] + reverse(rf_wr_t)

trace_t = [
    ["o", "valid"],
    ["o", "pc", 32],
    ["o", "instr", 16]
] + reverse(rf_wr_t)

dcache_op_t = [
    ["o", "req_flush"],
    ["o", "req_invalidate"],
    ["o", "req_writeback"],
    ["o", "req_prefetch"],
    ["o", "req_nofill"]
]

# FPU specific definitions
fpclass_t = [
    ["i", "is_zero"],
    ["i", "is_inf"],
    ["i", "is_nan"]
]

def fpnum_t(exp_bits, frac_bits):
    return [
        ["i", "sign"],
        ["i", "exp", exp_bits],
        ["i", "frac", frac_bits]
    ] + fpclass_t

# 4R2W regfile
define("REG_RD_PORTS", "4")
define("REG_WR_PORTS", "2")

define("FPREG_RD_PORTS", "3")
define("FPREG_WR_PORTS", "2")
