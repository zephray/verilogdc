//
// VerilogDC Co-simulator
// Copyright 2023 Wenting Zhang
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <signal.h> 

#include "washdc-cosim/external/sh4asm/sh4asm_core/disas.h"
#include "ibuf.hpp"

#include "verilated.h"
#include "verilated_vcd_c.h"

#include "Vsimtop.h"

#define CLK_PREIOD_PS       (5000) // 200 MHz
#define CLK_HALF_PERIOD_PS  (CLK_PREIOD_PS / 2)

// Cosim functions
void cosim_init(char *path_game, char const *dc_bios_path,
        char const *dc_flash_path, bool enable_serial, int64_t cycle_limit);
void cosim_run(void);
void cosim_finish(void);

// Verilator related
static Vsimtop *core;
static VerilatedVcdC *trace;
static uint64_t tickcount;

// Settings
static bool enable_trace = false;
static bool quiet = false;
static bool unlimited = true;
static uint64_t trace_from = 0;
static uint64_t max_cycles = 100;

double sc_time_stamp() {
    return (double)tickcount * (double)CLK_PREIOD_PS;
}

// static void sig_handler(int _) {
//     (void)_;
//     running = 0;
// }


// static void reset() {
//     core->rst = 0;
//     tick();
//     core->rst = 1;
//     for (int i = 0; i < 10; i++) {
//         tick();
//     }
//     core->rst = 0;
// }

typedef struct {
    uint32_t pc;
    uint16_t insn;
    uint8_t waddr;
    uint32_t wdata;
    bool wvalid;
    // The alt write port is for pointer self-increment only if there are 2
    // write targets.
    uint8_t alt_waddr;
    uint32_t alt_wdata;
    bool alt_wvalid;
    bool pending;
} retire_t;

typedef struct {
    int width;
    bool write;
    uint32_t addr;
    uint32_t data;
} memop_t;

static retire_t current_instruction;

#define INSTR_BUF_SIZE (256) 
static Ibuf<retire_t> retire_buf(INSTR_BUF_SIZE);

#define MEMOP_BUF_SIZE (128)
static Ibuf<memop_t> memop_buf(MEMOP_BUF_SIZE);

#define RETIRE_PRINT_BUF (64)
static Ibuf<retire_t> print_buf(RETIRE_PRINT_BUF);

static void print_instruction(retire_t *entry) {
    printf("PC %08x: %04x / %02d <- %08x [%d] / ",
            entry->pc, entry->insn, entry->waddr, entry->wdata, entry->wvalid);
    sh4asm_disas_inst(entry->insn, (sh4asm_disas_emit_func)putchar, entry->pc);
    printf("\n");
}

static void print_buffer() {
    retire_t ins;
    while (print_buf.pop(&ins)) {
        print_instruction(&ins);
    }
}

static void finish() {
    fprintf(stderr, "Simulation aborted at tick %llu.\n", tickcount);
    fprintf(stderr, "More EMU instructions for reference:\n");

    retire_t ref;
    int count = 0;
    
    while ((count < 8) && (retire_buf.pop(&ref))) {
        print_instruction(&ref);
        count++;
    }

    if (enable_trace) {
        trace->close();
        delete trace;
    }
    exit(0);
}

static void check_instruction(retire_t *instr) {
    retire_t ref;
    bool result = retire_buf.pop(&ref);
    // check pending bit here
    if ((!result) || (ref.pending)) {
        print_buffer();
        fprintf(stderr, "Ibuf empty!\n");
        finish();
    }
    if (!quiet) {
        print_buf.push(&ref);
    }
    if ((ref.pc != instr->pc) ||
            (ref.wvalid != instr->wvalid) ||
            (ref.wvalid && (ref.waddr != instr->waddr)) ||
            (ref.wvalid && (ref.wdata != instr->wdata))) {
        print_buffer();
        fprintf(stderr, "INSTRUCTION TRACE DIVERGE!\n");
        printf("DUT retired instruction:\n");
        print_instruction(instr);
        finish();
    }
}

static void tick() {
    // Create local copy of input signals
    uint32_t im_resp_rdata = 0;
    uint8_t im_resp_valid = 0;
    uint64_t dm_resp_rdata = 0;
    uint8_t dm_resp_valid = 0;

    static uint32_t no_retire_counter = 0;
    bool retired_inst = false;

    // Check IMEM request
    // Skip check if ibuf doesn't have anything, let it retry
    if ((core->im_req_valid) && (retire_buf.get_usage() > 5)) {
        bool found = false;
        im_resp_valid = 1;
        int rptr = retire_buf.rptr;
        int wptr = retire_buf.wptr;
        int size = retire_buf.size;
        while (rptr != wptr) {
            if (retire_buf.queue[rptr].pc == core->im_req_addr) {
                found = true;
                break;
            }
            rptr = (rptr + 1) % size;
        }
        if (found) {
            im_resp_rdata = (uint32_t)retire_buf.queue[rptr].insn;
            // Need to fill info to make it 32 bit
            if (core->im_req_addr & 0x3) {
                // Requesting high bits
                im_resp_rdata = im_resp_rdata << 16;
            }
            else {
                rptr = (rptr + 1) % size;
                if (retire_buf.queue[rptr].pc == core->im_req_addr + 2) {
                    // Matches
                    im_resp_rdata |= ((uint32_t)retire_buf.queue[rptr].insn) << 16;
                }
                // If doesn't match, probably doesn't matter???
            }
        }
        else {
            // Doesn't immediately fault
            im_resp_rdata = 0x0;
        }
    }

    // Check DM request
    if (core->dm_req_valid) {
        dm_resp_valid = 1;
        memop_t ref;
        bool result = memop_buf.pop(&ref);
        if (!result) {
            print_buffer();
            fprintf(stderr, "DUT requested memory access but EMU doesn't\n");
            finish();
        }
        
        uint8_t expected_mask;
        uint64_t bitmask;
        uint64_t expected_data;
        uint8_t shamt = 0;
        if (ref.width == 8) {
            shamt = (ref.addr & 0x7);
            expected_mask = 0x1 << shamt; 
            bitmask = 0xFFul << (shamt * 8);
        }
        else if (ref.width == 16) {
            shamt = ((ref.addr >> 1) & 0x3) * 2;
            expected_mask = 0x3 << shamt;
            bitmask = 0xFFFFul << (shamt * 8);
        }
        else if (ref.width == 32) {
            shamt = ((ref.addr >> 2) & 0x1) * 4;
            expected_mask = 0xf << shamt;
            bitmask = 0xFFFFFFFFul << (shamt * 8);
        }
        else if (ref.width == 64) {
            shamt = 0;
            expected_mask = 0xff;
            bitmask = 0xFFFFFFFFFFFFFFFFul;
        }
        else {
            assert(0);
        }
        expected_data = ref.data;
        uint64_t masked_req_wdata = (core->dm_req_wdata & bitmask) >> (shamt * 8);

        bool match = true;
        if (core->dm_req_wen) {
            // Write access
            if ((core->dm_req_addr != ref.addr) ||
                (masked_req_wdata != expected_data) ||
                (core->dm_req_wmask != expected_mask) ||
                (!ref.write)) {
                match = false;
            }
        }
        else {
            // Read access
            if ((core->dm_req_addr != ref.addr) || (ref.write)) {
                match = false;
            }
            dm_resp_rdata = expected_data << (shamt * 8);
        }

        if (!match) {
            print_buffer();
            fprintf(stderr, "Unable to match DMEM %s at address %08x\n",
                (core->dm_req_wen ? "write" : "read"), (core->dm_req_addr));
            
            fprintf(stderr, "EMU: ADDR %08x DATA %08x %d-bit %s\n",
                ref.addr, ref.data, ref.width, ref.write ? "WRITE" : "READ");
            if (core->dm_req_wen)
                fprintf(stderr, "DUT: ADDR %08x DATA %08lx MASK %02x WRITE \n",
                    core->dm_req_addr, masked_req_wdata, core->dm_req_wmask);
            else
                fprintf(stderr, "DUT: ADDR %08x READ\n", core->dm_req_addr);
            finish();
        }
    }

    // Check retired instruction
    retire_t retired;
    if ((core->trace_valid0 ^ core->trace_valid1) && (core->trace_wen0 && core->trace_wen1)) {
        // Case where the instruction has two write targets
        if (core->trace_valid0) {
            retired.pc = core->trace_pc0;
            retired.insn = core->trace_instr0;
        }
        else {
            retired.pc = core->trace_pc1;
            retired.insn = core->trace_instr1;
        }
        retired.waddr = core->trace_wdst0;
        retired.wdata = core->trace_wdata0;
        retired.wvalid = core->trace_wen0;
        retired.alt_waddr = core->trace_wdst1;
        retired.alt_wdata = core->trace_wdata1;
        retired.alt_wvalid = core->trace_wen1;
        check_instruction(&retired);
        retired_inst = true;
    }
    else {
        if (core->trace_valid0) {
            retired.pc = core->trace_pc0;
            retired.insn = core->trace_instr0;
            retired.waddr = core->trace_wdst0;
            retired.wdata = core->trace_wdata0;
            retired.wvalid = core->trace_wen0;
            check_instruction(&retired);
            retired_inst = true;
        }
        if (core->trace_valid1) {
            retired.pc = core->trace_pc1;
            retired.insn = core->trace_instr1;
            retired.waddr = core->trace_wdst1;
            retired.wdata = core->trace_wdata1;
            retired.wvalid = core->trace_wen1;
            check_instruction(&retired);
            retired_inst = true;
        }
    }

    if (retired_inst) {
        no_retire_counter = 0;
    }
    else {
        no_retire_counter++;
        if (no_retire_counter > 100) {
            print_buffer();
            fprintf(stderr, "The DUT CPU hasn't retired anything for a while!\n");
            finish();
        }
    }

    // Clock posedge
    core->clk = 1;
    core->eval();

    // Apply changed input signals after clock edge
    core->im_resp_rdata = im_resp_rdata;
    core->im_resp_valid = im_resp_valid;
    core->dm_resp_rdata = dm_resp_rdata;
    core->dm_resp_valid = dm_resp_valid;

    // Let combinational changes propergate
    core->eval();
    if (enable_trace && (tickcount >= trace_from))
        trace->dump(tickcount * CLK_PREIOD_PS);

    // Clk negedge
    core->clk = 0;    
    core->eval();
    if (enable_trace && (tickcount >= trace_from))
        trace->dump(tickcount * CLK_PREIOD_PS + CLK_HALF_PERIOD_PS);
    
    tickcount++;
}

void trace_reset(uint32_t boot_vector) {
    if (!quiet) {
        printf("DUT reset, vector %08x\n", boot_vector);
    }
    core->boot_vector = boot_vector;
    core->rst = 0;
    tick();
    core->rst = 1;
    for (int i = 0; i < 10; i++) {
        tick();
    }
    core->rst = 0;
}

static void trace_push_instruction(retire_t *instr) {
    if (!retire_buf.push(instr)) {
        print_buffer();
        fprintf(stderr, "Ibuf full!\n");
        finish();
    }
    while (retire_buf.get_usage() > 10) {
        tick();
    }
}

void trace_decode(uint32_t pc, uint16_t instr) {
    if (current_instruction.pending) {
        // Previous instruction doesn't provide register writeback, mark it has no wb and push
        current_instruction.wvalid = false;
        current_instruction.waddr = 0;
        current_instruction.wdata = 0;
        current_instruction.pending = false;
        trace_push_instruction(&current_instruction);
    }
    current_instruction.insn = instr;
    current_instruction.pc = pc;
    current_instruction.wvalid = false;
    current_instruction.pending = true;
}

void trace_regwrite(uint8_t reg, uint32_t val) {
    if (current_instruction.pending) {
        current_instruction.wvalid = true;
        current_instruction.waddr = reg;
        current_instruction.wdata = val;
        current_instruction.pending = false;
        trace_push_instruction(&current_instruction);
    }
    else {
        // Regwrite happened but no pending instruction, probably for those
        // with index self-incrementing, ignore for now
    }
}

void trace_memread(uint32_t addr, uint64_t data, int width) {
    memop_t memop;
    memop.write = false;
    memop.addr = addr;
    memop.data = data;
    memop.width = width;
    if (!memop_buf.push(&memop)) {
        print_buffer();
        fprintf(stderr, "Memop buf full!\n");
        finish();
    }
}

void trace_memwrite(uint32_t addr, uint64_t data, int width) {
    memop_t memop;
    memop.write = true;
    memop.addr = addr;
    memop.data = data;
    memop.width = width;
    if (!memop_buf.push(&memop)) {
        print_buffer();
        fprintf(stderr, "Memop buf full!\n");
        finish();
    }
}

int main(int argc, char *argv[]) {
    char *game_path = NULL;
    const char* dc_bios_path = "dc_bios.bin";
    const char* dc_flash_path = "dc_flash.bin";
    bool enable_serial = false;

    printf("VerilogDC Co-simulator\n");

    // Initialize testbench
    Verilated::commandArgs(argc, argv);

    core = new Vsimtop;
    Verilated::traceEverOn(true);

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], "--game") == 0) {
            if (i == (argc - 1)) {
                fprintf(stderr, "Error: no game filename provided\n");
                exit(1);
            }
            else {
                game_path = strdup(argv[i + 1]);
            }
        }
        else if (strcmp(argv[i], "--limit") == 0) {
            if (i == (argc - 1)) {
                fprintf(stderr, "Error: no cycle limit provided\n");
                exit(1);
            }
            else {
                max_cycles = atoll(argv[i + 1]);
                printf("Cycle limit set to %lu.\n", max_cycles);
                unlimited = false;
            }
        }
        else if (strcmp(argv[i], "--trace") == 0) {
            trace_from = 0;
            enable_trace = true;
        }
        else if (strcmp(argv[i], "--trace_from") == 0) {
            trace_from = atoll(argv[i + 1]);
            printf("Trace would only start after %lu cycles.\n", trace_from);
            enable_trace = true;
        }
        else if (strcmp(argv[i], "--serial") == 0) {
            enable_serial = true;
        }
    }

    if (!game_path) {
        fprintf(stderr, "No program image given! Specify using --game\n");
        exit(1);
    }

    if (enable_trace) {
        trace = new VerilatedVcdC;
        core->trace(trace, 99);
        trace->open("trace.vcd");        
    }

    // Initialize cosim
    cosim_init(game_path, dc_bios_path, dc_flash_path, enable_serial, max_cycles);

    // Start simulation
    if (!quiet)
        printf("Simulation start.\n");

    clock_t time = clock();

    //reset();

    //signal(SIGINT, sig_handler);

    cosim_run();

    if (!quiet)
        printf("Stop.\n");

    time = clock() - time;
    time /= (CLOCKS_PER_SEC / 1000);
    if (!quiet)
        printf("Executed %ld cycles in %ld ms. Average clock speed: %ld kHz\n",
                tickcount, time, tickcount / time);

    finish();

    cosim_finish();

    return 0;
}
