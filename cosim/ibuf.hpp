//
// VerilogDC Co-simulator
// Copyright 2023 Wenting Zhang
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

using namespace std;

template <typename T>
class Ibuf {
public:
    T *queue;
    int size;
    int rptr;
    int wptr;
    Ibuf(int s);
    int get_usage();
    bool push(T *entry);
    bool pop(T *entry);
};

template <typename T>
Ibuf<T>::Ibuf(int s) {
    queue = new T[s];
    size = s;
    rptr = 0;
    wptr = 0;
}

template <typename T>
int Ibuf<T>::get_usage() {
    int usage = wptr - rptr;
    if (usage < 0) usage += size;
    return usage;
}

template <typename T>
bool Ibuf<T>::push(T *entry) {
    int next_wptr = (wptr + 1) % size;
    bool rolled = false;
    if (next_wptr == rptr) {
        // Overflow, increment rptr to behave as a rolling buffer
        rptr = (rptr + 1) % size;
    }
    queue[wptr] = *entry;
    wptr = next_wptr;
    return !rolled;
}

template <typename T>
bool Ibuf<T>::pop(T *entry) {
    if (rptr == wptr) {
        return false; // empty
    }
    *entry = queue[rptr];
    rptr = (rptr + 1) % size;
    return true;
}
