//
// VerilogDC Co-simulator
// Copyright 2023 Wenting Zhang
//
// WashingtonDC Dreamcast Emulator
// Copyright (C) 2019, 2020, 2022 snickerbockers
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <stdio.h>
#include <stdint.h>

#include <iostream>
#include <string.h>

#include "washdc/hostfile.h"
#include "washdc/washdc.h"
#include "washdc/sound_intf.h"
#include "washdc/trace_intf.h"
#include "washdc/buildconfig.h"
#include "washdc/win.h"
#include "washdc_getopt.h"

#include "gfx_null.hpp"

#ifdef USE_LIBEVENT
#include "frontend_io/io_thread.hpp"
#endif

#ifdef ENABLE_DEBUGGER
#include "frontend_io/gdb_stub.hpp"
#include "frontend_io/washdbg_tcp.hpp"
#endif

#ifdef ENABLE_TCP_SERIAL
#include "frontend_io/serial_server.hpp"
#endif

#include "stdio_hostfile.hpp"
#include "paths.hpp"

static void null_sound_init(void);
static void null_sound_cleanup(void);
static void null_sound_submit_samples(washdc_sample_type *samples, unsigned count);

static struct washdc_hostfile_api hostfile_api;
static struct washdc_sound_intf snd_intf;
static struct washdc_trace_intf trace_intf;
static struct win_intf null_win_intf;

struct washdc_gameconsole const *console;

static void null_win_init(unsigned width, unsigned height);
static void null_win_check_events(void);
static void null_win_run_once_on_suspend(void);
static void null_win_update(void);
static void null_win_make_context_current(void);
static int null_win_get_width(void);
static int null_win_get_height(void);
static void null_win_update_title(void);

extern void trace_reset(uint32_t boot_vector);
extern void trace_decode(uint32_t pc, uint16_t instr);
extern void trace_regwrite(uint8_t reg, uint32_t val);
extern void trace_memread(uint32_t addr, uint64_t data, int width);
extern void trace_memwrite(uint32_t addr, uint64_t data, int width);

static bool strieq(char const *str1, char const *str2) {
    if (strlen(str1) != strlen(str2))
        return false;
    while (*str1 && *str2) {
        if (toupper(*str1++) != toupper(*str2++))
            return false;
    }
    return true;
}

void cosim_init(char *path_game, char const *dc_bios_path,
        char const *dc_flash_path, bool enable_serial, int64_t cycle_limit) {
    bool direct_boot = false;
    bool enable_debugger = false;
    bool enable_washdbg = false;
    bool enable_jit = false, enable_native_jit = false,
        enable_interpreter = true, inline_mem = true;
    bool log_stdout = false, log_verbose = false;
    struct washdc_launch_settings settings = { };
    bool write_to_flash_mem = false;

    create_cfg_dir();
    create_data_dir();
    create_screenshot_dir();

    settings.log_to_stdout = log_stdout;
    settings.log_verbose = log_verbose;
    settings.write_to_flash = write_to_flash_mem;

    hostfile_api.open = file_stdio_open;
    hostfile_api.close = file_stdio_close;
    hostfile_api.seek = file_stdio_seek;
    hostfile_api.tell = file_stdio_tell;
    hostfile_api.read = file_stdio_read;
    hostfile_api.write = file_stdio_write;
    hostfile_api.flush = file_stdio_flush;
    hostfile_api.open_cfg_file = open_cfg_file;
    hostfile_api.open_screenshot = open_screenshot;
#ifdef _WIN32
    hostfile_api.pathsep = '\\';
#else
    hostfile_api.pathsep = '/';
#endif

    settings.hostfile_api = &hostfile_api;

    if (enable_debugger && enable_washdbg) {
        fprintf(stderr, "You can't enable WashDbg and GDB at the same time\n");
        exit(1);
    }

    if (enable_debugger || enable_washdbg) {
        if (enable_jit || enable_native_jit) {
            fprintf(stderr, "Debugger enabled - this overrides the jit "
                    "compiler and sets WashingtonDC to interpreter mode\n");
            enable_jit = false;
            enable_native_jit = false;
        }
        enable_interpreter = true;

        if (washdc_have_debugger()) {
            settings.dbg_enable = true;
            settings.washdbg_enable = enable_washdbg;
        } else {
            fprintf(stderr, "ERROR: Unable to enable remote gdb stub.\n"
                    "Please rebuild with -DENABLE_DEBUGGER=On\n");
            exit(1);
        }
    } else {
        settings.dbg_enable = false;
    }

#ifdef ENABLE_DEBUGGER
    if (enable_debugger && !enable_washdbg)
        settings.dbg_intf = &gdb_frontend;
    else if (!enable_debugger && enable_washdbg)
        settings.dbg_intf = &washdbg_frontend;
#else
    enable_debugger = false;
    enable_washdbg = false;
#endif

    if (enable_interpreter && (enable_jit || enable_native_jit)) {
        fprintf(stderr, "ERROR: You can\'t use the interpreter and the JIT at "
                "the same time, silly!\n");
        exit(1);
    }

    settings.inline_mem = inline_mem;
    settings.enable_jit = enable_jit || enable_native_jit;

    direct_boot = false;
    if (path_game) {
        char const *ext = strrchr(path_game, '.');
        if (ext && (strieq(ext, ".bin") || strieq(ext, ".elf"))) {
            direct_boot = true;
            printf(".BIN OR .ELF FILE DETECTED; DIRECT-BOOT MODE ENABLED\n");
        }
    }

    if (direct_boot) {
        settings.boot_mode = WASHDC_BOOT_DIRECT;
        settings.path_1st_read_bin = path_game;
    } else {
        settings.boot_mode = WASHDC_BOOT_FIRMWARE;
    }

    if (dc_bios_path)
        settings.path_dc_bios = dc_bios_path;
    if (dc_flash_path)
        settings.path_dc_flash = dc_flash_path;
    settings.enable_serial = enable_serial;
    settings.path_gdi = direct_boot ? NULL : path_game;

    null_win_intf.check_events = null_win_check_events;
    null_win_intf.run_once_on_suspend = null_win_run_once_on_suspend;
    null_win_intf.update = null_win_update;
    null_win_intf.make_context_current = null_win_make_context_current;
    null_win_intf.update_title = null_win_update_title;
    null_win_intf.get_width = null_win_get_width;
    null_win_intf.get_height = null_win_get_height;

    settings.win_intf = &null_win_intf;

#ifdef ENABLE_TCP_SERIAL
    settings.sersrv = &sersrv_intf;
#endif

    snd_intf.init = null_sound_init;
    snd_intf.cleanup = null_sound_cleanup;
    snd_intf.submit_samples = null_sound_submit_samples;

    settings.sndsrv = &snd_intf;

    settings.gfx_rend_if = null_rend_if_get();

    trace_intf.reset = trace_reset;
    trace_intf.decode = trace_decode;
    trace_intf.regwrite = trace_regwrite;
    trace_intf.memread = trace_memread;
    trace_intf.memwrite = trace_memwrite;
    settings.tracesrv = &trace_intf;
    settings.cycle_limit = cycle_limit;

#ifdef USE_LIBEVENT
    io::init();
#endif

    null_win_init(640, 480); // made up fictional resolution

    console = washdc_init(&settings);
}

void cosim_run(void) {
    washdc_run();
}

void cosim_finish(void) {
#ifdef USE_LIBEVENT
    io::kick();
    io::cleanup();
#endif

    washdc_cleanup();
}

static void null_sound_init(void) {
}

static void null_sound_cleanup(void) {
}

static void null_sound_submit_samples(washdc_sample_type *samples, unsigned count) {
}

static int null_win_width, null_win_height;

static void null_win_init(unsigned width, unsigned height) {
    null_win_width = width;
    null_win_height = height;
}

static void null_win_check_events(void) {
}

static void null_win_run_once_on_suspend(void) {
}

static void null_win_update(void) {
}

static void null_win_make_context_current(void) {
}

static int null_win_get_width(void) {
    return null_win_width;
}

static int null_win_get_height(void) {
    return null_win_height;
}

static void null_win_update_title(void) {

}
